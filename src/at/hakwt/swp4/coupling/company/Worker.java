package at.hakwt.swp4.coupling.company;

public class Worker extends Employees{

    public void process(String[] instructions) {

        for (String instruction : instructions) {

            System.out.println("Working on " + instruction);
        }
    }
}
