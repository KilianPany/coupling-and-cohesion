package at.hakwt.swp4.coupling.company;

public class Manager {

    private Employees[] employees;

    public Manager(Employees[] e) {

        this.employees = e;
    }

    public void manageWork() {

        for (Employees e : this.employees) {

            e.process(new String[]{"this", "and", "that"});
        }

    }
}