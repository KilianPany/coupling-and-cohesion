package at.hakwt.swp4.coupling.company;

public class CompanyMain {

    public static void main(String[] args) {

        Worker karli = new Worker();
        Worker fredi = new Worker();

        Accountant kili= new Accountant();
        Accountant mary = new Accountant();

        Manager Robert = new Manager(new Worker[]{karli, fredi});
        Manager Dany = new Manager(new Accountant[]{kili, mary});

        Robert.manageWork();
        Dany.manageWork();
    }
}