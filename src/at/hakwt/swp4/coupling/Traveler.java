package at.hakwt.swp4.coupling;

public class Traveler {

    private  Vehicel vehicel;

    public Traveler(Vehicel v) {

       this.vehicel = v;
    }

    public void startJourney() {

        this.vehicel.move();
    }
}