package at.hakwt.swp4.coupling;

public class CouplingMain {

    public static void main(String[] args) {

	    Vehicel v1 = new Car();
        Vehicel v2 = new Bicycle();

        Traveler t = new Traveler(v1);
        Traveler t2 = new Traveler(v2);

	    t.startJourney();
        t2.startJourney();
    }
}
