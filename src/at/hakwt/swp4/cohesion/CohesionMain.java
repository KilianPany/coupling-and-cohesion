package at.hakwt.swp4.cohesion;

public class CohesionMain {

    public static void main(String[] args) {

        Traveler klaus = new Traveler();
        klaus.putCarinGarage();

        Car car = new Car();

        Garage myGarage = new Garage(0);
        myGarage.RepairCar(car);

        Restaurante waidhofen = new Restaurante(4);
        waidhofen.buyIngredients();
        klaus.eatMeal(waidhofen.cookMeal());
    }
}