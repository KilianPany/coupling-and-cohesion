package at.hakwt.swp4.cohesion;

public class Garage{

    private int maintainanceParts;

    public Garage(int maintainanceParts) {

        this.maintainanceParts = maintainanceParts;
    }

    private void buyMaintainanceParts() {

        if(maintainanceParts == 0) {

            System.out.println("Maintainance parts must be bought! You have " + maintainanceParts + " right Parts!");
        }else {

            System.out.println("You have enough parts to repair!");
        }

        System.out.println("Maintainance Parts have been bought!");
    }

    public void RepairCar(Car car) {
        buyMaintainanceParts();
        System.out.println("The garage is repairing the car!");
        System.out.println("The car is fixed!" + "\n");
    }
}