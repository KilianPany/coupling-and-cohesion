package at.hakwt.swp4.cohesion.payroll;

public class Expanse {

    private double value;

    public Expanse(double value) {

        this.value = value;
    }

    public double getValue() {

        return value;
    }
}