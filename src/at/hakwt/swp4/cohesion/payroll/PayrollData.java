package at.hakwt.swp4.cohesion.payroll;

public class PayrollData {

    private Employee employee;
    private Expanse[] expencies;
    private int overhours;

    public PayrollData(Employee employee, Expanse[] expencies, int overhours) {

        this.employee = employee;
        this.expencies = expencies;
        this.overhours = overhours;
    }

    public Employee getEmployee() {

        return employee;
    }

    public Expanse[] getExpencies() {

        return expencies;
    }

    public int getOverhours() {

        return overhours;
    }
}