package at.hakwt.swp4.cohesion.payroll;

public class PrintData {

    public void print(Employee employee, double amount, String month) {


        System.out.println("Mitarbeiter " + employee.getName() + " bekommt im " + month + " " + roundAmount(amount) + " Euro ausbezahlt!");
    }

    private double roundAmount(double amount){

        amount = Math.round(amount * 100.0)/100.0;      //Rundet den Betrag auf zwei Kommastellen

        return amount;
    }
}
