package at.hakwt.swp4.cohesion.payroll;

public class ImportDataFromCSV {

    public PayrollData importData(String fileName) {

        Expanse diaeten = new Expanse(53.8);
        Expanse kmGeld =new Expanse(100 * 0.42);
        Employee max = new Employee(1500, "Max Mustermnn");
        return new PayrollData(max, new Expanse[]{diaeten, kmGeld}, 3);
    }
}
