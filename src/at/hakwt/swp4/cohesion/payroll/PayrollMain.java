package at.hakwt.swp4.cohesion.payroll;

public class PayrollMain {

    public static void main(String[] args) {

        Employee ferdi = new Employee(2000,"Ferdinand");

        Expanse diaeten = new Expanse(26.90);
        Expanse kmGeld = new Expanse(150 * 0.42);
        Expanse[] expencies = new Expanse[]{diaeten, kmGeld};
        int noOfOverhours = 5;

        PayrollCalculator calculator = new PayrollCalculator();
        double amount = calculator.calculatePayment(ferdi, noOfOverhours, expencies);

        ImportDataFromCSV import1 = new ImportDataFromCSV();

        PayrollData data = import1.importData("data.csv");       //Es ist nur ein Test, es gibt keine data.csv Datei
        calculator.calculatePayment(data.getEmployee(), data.getOverhours(), data.getExpencies());

        PrintData pData = new PrintData();
        pData.print(data.getEmployee(), amount, "November 2022");

        //TODO: Verantwortlichkeit "Überstunden berechnen" im PayrollCalculater anpassen
    }
}