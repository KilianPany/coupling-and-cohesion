package at.hakwt.swp4.cohesion.payroll;

import java.util.Random;

public class PayrollCalculator {

    public double calculatePayment(Employee employee, int noOfOverhours, Expanse[] expencies) {

        double total = employee.getSalary();

        //calculate overhours value
        Random rnd = new Random();
        total += noOfOverhours * rnd.nextDouble();

        for (Expanse e: expencies) {

            total += e.getValue();
        }

        return total;
    }




}