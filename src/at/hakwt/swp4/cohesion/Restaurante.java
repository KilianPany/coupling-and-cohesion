package at.hakwt.swp4.cohesion;

public class Restaurante {

    private int ingredients;

    public Restaurante(int ingredients) {

        this.ingredients = ingredients;
    }

    public void buyIngredients() {

        if(ingredients == 0) {

            System.out.println("Ingredients must be bought! You have " + ingredients + " ingredients!");
        }else {

            System.out.println("You have enough ingredients to cook!");
        }

        System.out.println("Ingredients have been bought!");
    }

    public int cookMeal() {

        int meal = 0;

        System.out.println("The chef is cooking!");
        System.out.println("Your Meal is ready!" + "\n");

        return meal;
    }
}